#!/bin/env bash
#Created by madmax(Upanand Mahato)
#Created on Thu Jul 13 23:17:50 IST 2023
#Project: wrapper script for ytfzf in termux
#
#
# ##########################################################################
# Copyright (C) 2023 Upanand Mahato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation,either version 3 of the License,or
# (at your option)any later version.
#
# This program is distributed in the hope that it will be useful,but
# WITHOUT ANY WARRANTY;without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARITCULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of GNU General Public License
# along with this program. If not,see <https://www.gnu.org/licenses/>.#
###########################################################################

#
# Download directory
dir_name=Youtube

# FUNCTIONS
# Get exit code of termux-dialog
excode () {
	jq -r '.code' | sed s/-//g
}

# Exit dialog when exit code is received
term_on_exit () {
	if [[ $exit_code == 2 ]];
	then exit
	fi
}

# Get the text output
text_get () {
	jq -r '.text'
}

# Hack fix While loop to fix orientation bug of termux-dialog
fix_ori () {
	while [[ $string == "" ]];
	do
		$1
	done
}

# Search
search_diag () {
	main_var=$(termux-dialog -t "Enter search term")
	string=$(echo $main_var | text_get)
	exit_code=$(echo $main_var | excode)
	term_on_exit
}

# Download or play dialog
dl_play () {
	main_var=$(termux-dialog radio -v "Download,Play" \
		   -t "Select the action")
	string=$(echo $main_var | text_get)
	exit_code=$(echo $main_var | excode)
	term_on_exit
}

# Download 
download () {
	yt-dlp --embed-metadata -S "res:$string,vext:webm,acodec:opus" ${url}
}

# Play audio only
play_audio () {
	mpv ${url}
}

# Select resolution
res_select () {
	main_var=$(termux-dialog radio -v "1080,720,480,360,240,144"\
		   -t "Select resolution")
	string=$(echo $main_var | text_get)
	exit_code=$(echo $main_var | excode)
	term_on_exit
}

# Search string
search_diag;fix_ori search_diag

# search youtube using ytfzf
url=$(ytfzf -I l -t --skip-thumb-download -T chafa --preview-side=up --pages=3 "$string")

# Download or play
dl_play; fix_ori dl_play

# Use ytdlp to download or play audio using mpv 
if [[ $string == "Download" ]];
then	res_select; download
elif [[ $string == "Play" ]];
then	play_audio
fi


# Remove spaces from filename
rename -av ' ' _ *.webm

# Get filename
filename=$(find *.webm)

# move files to the appropriate directory
mv ${filename} ~/storage/shared/${dir_name}/

# Open files using set default video player
xdg-open ~/storage/shared/${dir_name}/${filename}
exit
