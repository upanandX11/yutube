#!/bin/env bash
#Created by madmax(Upanand Mahato)
#Created on Fri Jul 14 20:38:13 IST 2023
#Project: install script for yutube.sh
#
#
############################################################################
# Copyright (C) 2023 Upanand Mahato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation,either version 3 of the License,or
# (at your option)any later version.
#
# This program is distributed in the hope that it will be useful,but
# WITHOUT ANY WARRANTY;without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARITCULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of GNU General Public License
# along with this program. If not,see <https://www.gnu.org/licenses/>.
###########################################################################
#
#
# color codes
RED="\e[1;31m"
GREEN="\e[1;32m"
NORMAL="\e[0m"

# check the availability of packages
check_install () {
	if [[ -f ${PREFIX}/bin/$1 ]];
	then echo -e status:"${GREEN}"OK${NORMAL}
	else echo -e status:"${RED}"Not_installed${NORMAL}
	fi
}

# Intall the dependencies
pkg install jq fzf termux-api ytfzf python python-pip -y

# Install yt-dlp python package
echo ''
echo 'Installing yt-dlp python package...'
pip install yt-dlp

# Install additional packages
echo 'Installing additional packages...'
pkg install vim micro wget mpv -y

# Downloadimg termux addons
# termux-widget
echo 'Downloading termux addons'
echo ''
echo 'Downloading termux-widget...'
wget -nv -O $HOME/storage/downloads/termux-widget.apk \
	https://f-droid.org/repo/com.termux.widget_13.apk
# termux-api
echo ''
echo 'Downloading termux-api...'
wget -nv -O $HOME/storage/downloads/termux-api.apk \
	https://f-droid.org/repo/com.termux.api_51.apk
# termux-styling
echo ''
echo 'Downloading termux-styling...'
wget -nv -O $HOME/storage/downloads/termux-styling.apk \
	https://f-droid.org/repo/com.termux.styling_30.apk
echo 'Install these APKs from the downloads 
directory.'
sleep 4
echo ''
# Initial setup
echo 'Creating directories'
mkdir -p $HOME/.shortcuts
mkdir -p $HOME/storage/shared/Youtube
echo ''
echo 'Copying the files'
cp $HOME/yutube/yutube.sh $HOME/.shortcuts/
# Verifying installation
echo ''
echo 'Verifying install..'
sleep 5
echo 'Package:jq';check_install jq
echo ''
echo 'Package:fzf';check_install fzf
echo ''
echo 'Package:ytfzf';check_install ytfzf
echo ''
echo 'Package:termux-api-start';check_install termux-api-start
echo ''
echo 'Package:termux-api-stop';check_install termux-api-stop
echo ''
echo 'Package:python';check_install python
echo ''
echo 'Package:yt-dlp';check_install yt-dlp
echo ''
echo 'Package:vim';check_install vim
echo ''
echo 'Package:micro';check_install micro
echo ''
echo 'Package:wget';check_install wget
echo ''
echo 'Package:mpv';check_install mpv
echo ''

# Colours to highlight important stuff.
Status=$(echo -e status:"${RED}"Not_installed${NORMAL})
Note="\e[1;96mNote\e[0m"
install="\e[1;32minstall\e[0m"

echo -e \
"${Note}: If you saw ${Status}
on any package then you can install it by
running 'pkg $install <package-name>'.
And yt-dlp is a python package and to
install it run 'pip $install yt-dlp'. 
Also python-pip is required to run pip.
To install it run 'pkg $install python-pip."
